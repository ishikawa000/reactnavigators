日付: 2017-10-05  
作成者: eflow 石川陽一郎


# 概要
　本プロジェクトはReactNativeアプリを作成するためのhow-toを集める一環として作られ、
fluxの観点で、
`TypeScript + react-native + react-redux`を用いて作られています。

　ここでは、実際にこのアプリを動かすまでの手順を説明します。


# このドキュメント作成時点の私の環境

|               |                           |
|---------------|---------------------------|
| yarn          | 1.1.0                     |
| AndroidStudio | 2.3.3                     |
| mintty        | 2.7.9 (x86\_64-pc-cygwin) |

- ※ここではAndroidStudioはAndroidVirtualDevicesを導入するためのみに使われます
- ※私の手元の環境は`Windows10 + Cygwin`ですが、他の環境でもほぼ同じ手順で、アプリが実行できるはずです


# fluxについての参考資料
![flux-image](http://www.infoq.com/resource/news/2014/05/facebook-mvc-flux/ja/resources/flux-react.png)

- [flux/examples/flux-concepts at master - facebook/flux - GitHub](https://github.com/facebook/flux/tree/master/examples/flux-concepts)
- [React.js・ReactNative・Redux入門](https://www.slideshare.net/kazuhiroyoshimoto58/reactjsreactnativeredux)
- [Fluxとはなんなのか - Qiita](https://qiita.com/knhr__/items/5fec7571dab80e2dcd92)
- [ReactとFluxのこと // Speaker Deck](https://speakerdeck.com/geta6/reacttofluxfalsekoto)


# ReactNativeについての参考資料
- [ReactNative](https://facebook.github.io/react-native/)
    - [Getting Started - React Native](https://facebook.github.io/react-native/docs/getting-started.html)
    - [Tutorial - React Native](https://facebook.github.io/react-native/docs/tutorial.html#content)
- [主に備忘録: Buttonコンポーネントを使ってみる](http://st--blog.blogspot.jp/2017/01/hello.html)


# TypeScript + react-native + react-reduxについての参考資料
- [React + Redux + TypeScriptの最小構成 - Qiita](https://qiita.com/uryyyyyyy/items/3ad88cf9ca9393335f8c)
- [React Native + TypeScript入門 - Qiita](https://qiita.com/uryyyyyyy/items/163027b4fb8606f4cdff)


# 手順
## 導入
### yarnをインストールする

- [Installation | Yarn](https://yarnpkg.com/lang/en/docs/install/#mac-tab)

　例えばChocolate導入済みのWindowsでは以下のコマンドでインストールできます。

```console
> choco install yarn
```

### AndroidStudioをインストールし、AVDを導入する

- [Android Studio と SDK ツールをダウンロードする | Android Studio](https://developer.android.com/studio/index.html?hl=ja)

### 本プロジェクトの環境を整える
　以下のコマンドを実行する。

```console
$ git clone https://bitbucket.org/ishikawa000/reactnavigators
$ cd reactnavigators
$ yarn install
```


## 実行
### エミュレータの起動
　AVDで仮想Androidデバイスを作成し、エミュレータで起動します。

　現行ではAVDの実行ファイルは、Windowsの場合以下にあり

- /Users/ishikawa/AppData/Local/Android/sdk/tools/emulator

以下のように実行すると、Androidエミュレータにより仮想Androidデバイスが起動します。

```console
$ emulator -avd {作成した仮想デバイス名}
```

- Windowsでの例
    - `$ emulator -avd Nexus_5X_API_25`

### アプリの実行

```console
$ yarn build
$ yarn run android
```


# 追伸
　`$ yarn run android`実行後に、
仮想デバイスにExpoというアプリがインストールされるはずなので、
アプリのホットローディングの調子がおかしいときは、
アプリのタスクを仮想デバイス上などからkillして、
Expoアプリから再度開くなどしてください。
