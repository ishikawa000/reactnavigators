/*
 * In the comment of this code,
 *  "ACTION", "DISPATCHER", "STORE", and "VIEW"
 *  means flux's things
 */

import * as React from 'react';
import {Component} from 'react';
import {AppRegistry, Text, View, Button, ToastAndroid} from 'react-native';
import {Provider, connect} from 'react-redux';
import {createStore, Action, Dispatch} from 'redux'

/**
 * A tag for AppProps#targetView
 */
enum ScreenMode {
    LEFT,
    CENTER,
    RIGHT,
}

/**
 * ???
 */
interface ScreenState {}

/**
 * The initial value of `ScreenState`
 * ???
 */
const initialState: ScreenState = {};

/**
 * A tag of the "ACTION".
 * The "DISPATCHER" pass this to "STORE".
 *
 * flux branches "STORE"'s function by this... maybe :P
 */
interface IntentAction extends Action {
    type: 'to-left' | 'to-center' | 'to-right';
}

/**
 * The flux's "DISPATCHER" gives an `IntentAction` as flux's "ACTION".
 * This gives the state of the result to "VIEW".
 *
 * In this app, when `AppView`'s buttons are pressed,
 * a `ScreenMode` is passed to `AppDispatcher`.
 * And `AppDispatcher`'s result is notified to this.
 *
 * Also this gives the result to `AppView` as its props.
 */
function reducer(state: ScreenState = initialState, action: IntentAction): ScreenState {
    switch (action.type) {
    case 'to-left':
        return ScreenMode.LEFT;
    case 'to-center':
        return ScreenMode.CENTER;
    case 'to-right':
        return ScreenMode.RIGHT;
    }
}

/**
 * A reducer's representation as "STORE"
 */
const store = createStore(reducer);

/**
 * A flux component.
 * This includes all flux stuff ("VIEW", "ACTION", "DISPATCHER", and "STORE").
 *
 * This is the whole of app,
 * the users sees this in the real world.
 */
export default class AppSurface extends Component<{}, {}> {
  render() {
    return (
        <Provider store={store}>
            <App/>
        </Provider>
    );
  }
}

AppRegistry.registerComponent('ReactNavigators', () => AppSurface);

/**
 * The props for AppView.
 * This is like the state, but redux represents the state by the props.
 *
 * 'targetView': the current view of the AppView's text
 */
interface AppProps {
    targetView: ScreenMode;
    dispatcher: AppDispatcher;
}

/**
 * A "VIEW" of the app.
 *
 * Happen "ACTION"s, it is passed to the "DISPATCHER" (in this app, it is `AppDispatcher`).
 */
class AppView extends Component<AppProps, {}> {
    render() {
        return (
            <View>
                <Text>hi</Text>
                <Text>I am in the {this.props.targetView}</Text>
                <View>
                    <Button title='to left' onPress={() => this.props.dispatcher.intentTo(ScreenMode.LEFT)}/>
                    <Button title='to center' onPress={() => this.props.dispatcher.intentTo(ScreenMode.CENTER)}/>
                    <Button title='to right' onPress={() => this.props.dispatcher.intentTo(ScreenMode.RIGHT)}/>
                </View>
            </View>
        );
    }
}

/**
 * A part of `AppProps` for `AppProps#targetView`.
 * ???
 */
type AppPropsState = {targetView: ScreenMode};

/**
 * A convertion for `App` and `connect`.
 * ???
 */
function stateToProps(mode: ScreenMode): AppPropsState {
    return {targetView: mode};
}

/**
 * The part of AppProps, for dispatcherToProps.
 * This instance is published to AppView's props.dispatcher .
 */
type AppPropsAction = {dispatcher: AppDispatcher};

/**
 * The "DISPATCHER", for `AppProps`.
 * This is provided by `dispatcherToProps` to `AppView`.
 */
class AppDispatcher {
    /**
     * Take a dispatch function,
     * it maybe taken by react-redux.connect in mostly case
     */
    constructor (private dispatch: (_: Action) => void) {}

    //NOTE: Should this is put onto `AppView` ? (Why any examples puts this on here ?)
    /**
     * Make an "ACTION"
     */
    intentTo_(mode: ScreenMode): IntentAction {
        switch (mode) {
        case ScreenMode.LEFT:
            return {type: 'to-left'};
        case ScreenMode.CENTER:
            return {type: 'to-center'};
        case ScreenMode.RIGHT:
            return {type: 'to-right'};
        }
    }

    /**
     * The "DISPATCHER" function.
     * Make the "ACTION", and Dispatch it.
     */
    intentTo(target: ScreenMode) {
        this.dispatch(this.intentTo_(target));
    }
}

/**
 * A convertion for `App` and `connect`.
 * Pass a dispatch function to `AppPropsAction`.
 *
 * @param dispatch the dispatch function
 */
function dispatcherToProps(dispatch: Dispatch<void>): AppPropsAction {
    return {dispatcher: new AppDispatcher(dispatch)};
}

/**
 * A component, includes "VIEW", "ACTION", and "DISPATCHER"
 */
const App = connect(stateToProps, dispatcherToProps)(AppView);
